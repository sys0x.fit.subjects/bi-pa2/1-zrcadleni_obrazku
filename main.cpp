#ifndef __PROGTEST__

#include <cstdio>
#include <cstdlib>
#include <cstdint>
#include <cstring>
#include <cassert>
#include <cmath>
#include <cctype>
#include <climits>

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <vector>
#include <map>
#include <set>
#include <unordered_map>
#include <unordered_set>
#include <list>
#include <algorithm>
#include <functional>
#include <memory>

using namespace std;

const uint16_t ENDIAN_LITTLE = 0x4949;
const uint16_t ENDIAN_BIG = 0x4d4d;

#endif /* __PROGTEST__ */

class PIXEL {
public:
    char *bytes;
};

class IMAGE {
public:

    uint16_t width;
    uint16_t high;
    uint16_t image_size;
    uint16_t pixel_size;

    PIXEL *pixels;
};


IMAGE image;
char header[8];

bool readImage(const char *srcFileName) {

  ifstream srcImage = ifstream(srcFileName, ios::binary);
  if (!srcImage) {
    //cerr << srcFileName << endl;
    return false;
  }

  srcImage.read(header, 8);

  if (!srcImage.good()) {

    return false;
  }


  uint16_t endian;
  uint16_t width;
  uint16_t high;
  uint16_t pixel_data;
  uint16_t channel_num;
  uint16_t channel_size;

  endian = static_cast<uint16_t>((header[0] << 8) + header[1]);

  int x = 0;
  if (endian == ENDIAN_BIG) {
    x = 1;
  } else if (endian != ENDIAN_LITTLE) {
    return false;
  }

  width = static_cast<uint16_t>((header[3 - x] << 8) + header[2 + x]);

  high = static_cast<uint16_t>((header[5 - x] << 8) + header[4 + x]);

  if (width <= 0 || high <= 0) {
    return false;
  }

  pixel_data = static_cast<uint16_t>((header[7 - x] << 8) + header[6 + x]);

  channel_num = static_cast<uint16_t>(pixel_data & 3);
  if (channel_num == 0) {
    channel_num = 1;
  } else if (channel_num == 2) { channel_num = 3; }
  else if (channel_num == 3) { channel_num = 4; }
  else
    return false;

  channel_size = pixel_data >> 2;
  if (channel_size == 3) {
    channel_size = 1;
  } else if (channel_size == 4) {
    channel_size = 2;
  } else
    return false;

  int image_size = width * high;
  image.pixels = new PIXEL[image_size+10];
  image.width = width;
  image.high = high;
  image.image_size = width * high;
  image.pixel_size = channel_num * channel_size;


  int i = 0;

  while (srcImage.good()) {
    if(i!=image_size) {
      image.pixels[i].bytes = new char[image.pixel_size];
      srcImage.read(image.pixels[i].bytes, image.pixel_size);
    }
    if ( i > image.image_size) {
      return false;
    }
    i++;
  }

  if (i != (image.image_size + 1)) {
    return false;
  }
  srcImage.close();
  return true;
}

PIXEL *flipPixelsVertical(PIXEL *pixels) {
  PIXEL *flippedPixels = new PIXEL[image.image_size];

  int k = 0;
  for (int i = 0; i < image.high; i++) {
    for (int j = image.width * (i + 1) - 1; j >= image.width * i; j--) {
      flippedPixels[k] = pixels[j];
      k++;
    }
  }

  return flippedPixels;

}

PIXEL *flipPixelsHorizontal(PIXEL *pixels) {
  PIXEL *flippedPixels = new PIXEL[image.image_size];

  int k = 0;
  for (int i = image.high; i > 0; i--) {
    for (int j = 0; j < image.width; j++) {
      int whereToPut = (i - 1) * image.width + j;
      flippedPixels[k] = pixels[whereToPut];
      k++;
    }
  }

  return flippedPixels;
}

bool flipImage(const char *srcFileName,
               const char *dstFileName,
               bool flipHorizontal,
               bool flipVertical) {

  if (!readImage(srcFileName)) {
    return false;
  }

  if (flipHorizontal) {
    image.pixels = flipPixelsVertical(image.pixels);
  }

  if (flipVertical) {
    image.pixels = flipPixelsHorizontal(image.pixels);

  }

  ofstream newFile;
  newFile.open(dstFileName, ios::binary);
  if (!newFile.good()) {
    return false;
  }



  newFile.write(header, 8);

  if (!newFile.good()) {
    return false;
  }


  for (int i = 0; i < image.image_size; i++) {
    newFile.write(image.pixels[i].bytes, image.pixel_size);
    if (!newFile.good()) {
      return false;
    }
  }


  newFile.close();
  cout<<"done: "<<srcFileName<<endl;
  return true;
}

#ifndef __PROGTEST__


  bool identicalFiles(const std::string& p1, const std::string& p2) {
    std::ifstream f1(p1, std::ifstream::binary|std::ifstream::ate);
    std::ifstream f2(p2, std::ifstream::binary|std::ifstream::ate);

    if (f1.fail() || f2.fail()) {
      return false; //file problem
    }

    if (f1.tellg() != f2.tellg()) {
      return false; //size mismatch
    }

    //seek back to beginning and use std::equal to compare contents
    f1.seekg(0, std::ifstream::beg);
    f2.seekg(0, std::ifstream::beg);
    return std::equal(std::istreambuf_iterator<char>(f1.rdbuf()),
                      std::istreambuf_iterator<char>(),
                      std::istreambuf_iterator<char>(f2.rdbuf()));
  }

int main ( void )
{
  assert ( flipImage ( "input_00.img", "output_00.img", true, false )
           && identicalFiles ( "output_00.img", "ref_00.img" ) );

  assert ( flipImage ( "input_01.img", "output_01.img", false, true )
           && identicalFiles ( "output_01.img", "ref_01.img" ) );

  assert ( flipImage ( "input_02.img", "output_02.img", true, true )
           && identicalFiles ( "output_02.img", "ref_02.img" ) );

  assert ( flipImage ( "input_03.img", "output_03.img", false, false )
           && identicalFiles ( "output_03.img", "ref_03.img" ) );

  assert ( flipImage ( "input_04.img", "output_04.img", true, false )
           && identicalFiles ( "output_04.img", "ref_04.img" ) );

  assert ( flipImage ( "input_05.img", "output_05.img", true, true )
           && identicalFiles ( "output_05.img", "ref_05.img" ) );

  assert ( flipImage ( "input_06.img", "output_06.img", false, true )
           && identicalFiles ( "output_06.img", "ref_06.img" ) );

  assert ( flipImage ( "input_07.img", "output_07.img", true, false )
           && identicalFiles ( "output_07.img", "ref_07.img" ) );

  assert ( flipImage ( "input_08.img", "output_08.img", true, true )
           && identicalFiles ( "output_08.img", "ref_08.img" ) );

  assert ( ! flipImage ( "input_09.img", "output_09.img", true, false ) );

  // extra inputs (optional & bonus tests)
  assert ( flipImage ( "extra_input_00.img", "extra_out_00.img", true, false )
           && identicalFiles ( "extra_out_00.img", "extra_ref_00.img" ) );
  assert ( flipImage ( "extra_input_01.img", "extra_out_01.img", false, true )
           && identicalFiles ( "extra_out_01.img", "extra_ref_01.img" ) );
  assert ( flipImage ( "extra_input_02.img", "extra_out_02.img", true, false )
           && identicalFiles ( "extra_out_02.img", "extra_ref_02.img" ) );
  assert ( flipImage ( "extra_input_03.img", "extra_out_03.img", false, true )
           && identicalFiles ( "extra_out_03.img", "extra_ref_03.img" ) );
  assert ( flipImage ( "extra_input_04.img", "extra_out_04.img", true, false )
           && identicalFiles ( "extra_out_04.img", "extra_ref_04.img" ) );
  assert ( flipImage ( "extra_input_05.img", "extra_out_05.img", false, true )
           && identicalFiles ( "extra_out_05.img", "extra_ref_05.img" ) );
  assert ( flipImage ( "extra_input_06.img", "extra_out_06.img", true, false )
           && identicalFiles ( "extra_out_06.img", "extra_ref_06.img" ) );
  assert ( flipImage ( "extra_input_07.img", "extra_out_07.img", false, true )
           && identicalFiles ( "extra_out_07.img", "extra_ref_07.img" ) );
  assert ( flipImage ( "extra_input_08.img", "extra_out_08.img", true, false )
           && identicalFiles ( "extra_out_08.img", "extra_ref_08.img" ) );
  assert ( flipImage ( "extra_input_09.img", "extra_out_09.img", false, true )
           && identicalFiles ( "extra_out_09.img", "extra_ref_09.img" ) );
  assert ( flipImage ( "extra_input_10.img", "extra_out_10.img", true, false )
           && identicalFiles ( "extra_out_10.img", "extra_ref_10.img" ) );
  assert ( flipImage ( "extra_input_11.img", "extra_out_11.img", false, true )
           && identicalFiles ( "extra_out_11.img", "extra_ref_11.img" ) );
  return 0;
}
#endif /* __PROGTEST__ */
